package com.fq.rpc.service;

/**
 * @author jifang
 * @since 16/8/4 上午10:56.
 */
public interface IHelloService {

    String sayHello(String name);

    String sayGoodBye(String name);
}
