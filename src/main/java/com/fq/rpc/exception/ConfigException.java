package com.fq.rpc.exception;

/**
 * @author jifang
 * @since 16/8/14 上午10:35.
 */
public class ConfigException extends RuntimeException {

    public ConfigException(String message, Throwable cause) {
        super(message, cause);
    }
}
