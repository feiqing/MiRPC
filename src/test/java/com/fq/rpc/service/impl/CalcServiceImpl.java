package com.fq.rpc.service.impl;

import com.fq.rpc.service.ICalcService;

/**
 * @author jifang
 * @since 16/8/14 上午10:44.
 */
public class CalcServiceImpl implements ICalcService {

    @Override
    public Long add(Long a, Long b) {
        return a + b;
    }

    @Override
    public Long minus(Long a, Long b) {
        return a - b;
    }
}
