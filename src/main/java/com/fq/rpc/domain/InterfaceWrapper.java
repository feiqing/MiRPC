package com.fq.rpc.domain;

import java.io.Serializable;

/**
 * @author jifang
 * @since 16/8/14 上午11:28.
 */
public class InterfaceWrapper implements Serializable {

    private Class<?> inter;

    private String methodName;

    private Class<?>[] paramTypes;

    private Object[] arguments;

    public Class<?> getInter() {
        return inter;
    }

    public void setInter(Class<?> inter) {
        this.inter = inter;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class<?>[] getParamTypes() {
        return paramTypes;
    }

    public void setParamTypes(Class<?>[] paramTypes) {
        this.paramTypes = paramTypes;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public void setArguments(Object[] arguments) {
        this.arguments = arguments;
    }
}
