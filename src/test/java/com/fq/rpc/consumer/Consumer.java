package com.fq.rpc.consumer;

import com.fq.rpc.ConsumerClient;
import com.fq.rpc.service.ICalcService;
import com.fq.rpc.service.IHelloService;
import org.junit.Test;

/**
 * @author jifang
 * @since 16/8/4 上午11:35.
 */
public class Consumer {

    @Test
    public void consumer() {
        IHelloService helloService = ConsumerClient.getInstance().subscribe(IHelloService.class);
        ICalcService calcService = ConsumerClient.getInstance().subscribe(ICalcService.class);

        String hello = helloService.sayHello("翡青");
        System.out.println(hello);
        String goodBye = helloService.sayGoodBye("翡青");
        System.out.println(goodBye);
        long add = calcService.add(1L, 1L);
        System.out.println(add);
        long minus = calcService.minus(1L, 1L);
        System.out.println(minus);
    }
}
