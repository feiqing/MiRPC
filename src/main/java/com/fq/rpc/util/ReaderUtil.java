package com.fq.rpc.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author jifang
 * @since 16/8/14 下午3:32.
 */
public class ReaderUtil {

    private static final int BUF_SIZE = 8192;

    private static byte[] BUFFER = new byte[BUF_SIZE];

    public static byte[] toByteArray(InputStream in) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        int count;
        do {
            count = in.read(BUFFER);
            out.write(BUFFER, 0, count);
        } while (count == BUF_SIZE);

        return out.toByteArray();
    }
}
