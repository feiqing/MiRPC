package com.fq.rpc;

import com.fq.rpc.core.ServerBootstrap;
import com.fq.rpc.task.ConsumerIInvokeTask;
import com.fq.rpc.util.ServiceImplMap;
import com.google.common.base.Strings;

import java.lang.reflect.Proxy;

/**
 * @author jifang
 * @since 16/8/4 上午11:00.
 */
public class ProducerClient {

    static {
        new Thread(new ServerBootstrap()).start();
    }

    private static ProducerClient instance = new ProducerClient();

    public static ProducerClient getInstance() {
        return instance;
    }

    public void publish(Class<?> inter, Object impl) {
        ServiceImplMap.push(inter, impl);
    }
}
