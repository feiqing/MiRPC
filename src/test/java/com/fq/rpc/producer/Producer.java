package com.fq.rpc.producer;

import com.fq.rpc.ProducerClient;
import com.fq.rpc.service.ICalcService;
import com.fq.rpc.service.IHelloService;
import com.fq.rpc.service.impl.CalcServiceImpl;
import com.fq.rpc.service.impl.HelloServiceImpl;
import org.junit.Test;

import java.io.IOException;

/**
 * @author jifang
 * @since 16/8/4 上午11:36.
 */
public class Producer {

    private static final int _1H = 1000 * 60 * 60;

    @Test
    public void producer() throws IOException, InterruptedException {
        IHelloService helloService = new HelloServiceImpl();
        ICalcService calcService = new CalcServiceImpl();
        ProducerClient.getInstance().publish(IHelloService.class, helloService);
        ProducerClient.getInstance().publish(ICalcService.class, calcService);

        Thread.sleep(_1H);
    }
}
