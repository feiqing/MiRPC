package com.fq.rpc.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author jifang
 * @since 16/8/4 上午10:58.
 */
public class ThreadPoolInstance {

    private static ExecutorService executor = Executors.newFixedThreadPool(20);

    public static ExecutorService getExecutor() {
        return executor;
    }
}
