package com.fq.rpc.exception;

/**
 * @author jifang
 * @since 16/8/14 上午11:19.
 */
public class MiRPCException extends RuntimeException{

    public MiRPCException(String message) {
        super(message);
    }

    public MiRPCException(String message, Throwable cause) {
        super(message, cause);
    }

    public MiRPCException(Throwable cause) {
        super(cause);
    }
}
