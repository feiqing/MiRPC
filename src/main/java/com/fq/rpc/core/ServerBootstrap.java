package com.fq.rpc.core;

import com.fq.rpc.task.ProducerInvokeTask;
import com.fq.rpc.util.ServerSocketInstance;
import com.fq.rpc.util.ThreadPoolInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * @author jifang
 * @since 16/8/14 上午10:52.
 */
public class ServerBootstrap implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerBootstrap.class);

    @Override
    public void run() {
        ServerSocket server = ServerSocketInstance.getServerSocket();

        try {
            while (true) {
                Socket client = server.accept();
                ThreadPoolInstance.getExecutor().submit(new ProducerInvokeTask(client));
            }
        } catch (IOException e) {
            LOGGER.error("accept error: ", e);
        }
    }
}
