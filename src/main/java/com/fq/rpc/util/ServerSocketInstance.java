package com.fq.rpc.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * @author jifang
 * @since 16/8/14 上午10:32.
 */
public class ServerSocketInstance {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerSocketInstance.class);

    private static ServerSocket server;
    
    static {
        try {
            server = new ServerSocket(0);
        } catch (IOException e) {
            LOGGER.error("new ServerSocket Error", e);
        }
    }

    public static ServerSocket getServerSocket() {
        return server;
    }
}
