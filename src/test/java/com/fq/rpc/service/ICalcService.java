package com.fq.rpc.service;

/**
 * @author jifang
 * @since 16/8/14 上午10:44.
 */
public interface ICalcService {

    Long add(Long a, Long b);

    Long minus(Long a, Long b);
}
