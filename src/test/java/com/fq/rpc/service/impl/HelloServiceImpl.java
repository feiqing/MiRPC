package com.fq.rpc.service.impl;

import com.fq.rpc.service.IHelloService;

/**
 * @author jifang
 * @since 16/8/4 上午10:56.
 */
public class HelloServiceImpl implements IHelloService {

    @Override
    public String sayHello(String name) {
        return String.format("Hello %s.", name);
    }

    @Override
    public String sayGoodBye(String name) {
        return String.format("%s Good Bye.", name);
    }
}
