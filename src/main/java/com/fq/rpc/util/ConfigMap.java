package com.fq.rpc.util;

import com.fq.rpc.exception.ConfigException;

import java.io.IOException;
import java.util.Properties;

/**
 * @author jifang
 * @since 16/8/14 上午11:50.
 */
public class ConfigMap {

    public static String getServer(String key) {
        return serverConfig.getProperty(key);
    }

    public static String getClient(String key) {
        return clientConfig.getProperty(key);
    }

    private static Properties serverConfig = loadClientConfig("server.properties");

    private static Properties clientConfig = loadClientConfig("client.properties");

    private static Properties loadClientConfig(String filename) {
        Properties config = new Properties();
        try {
            config.load(ServerSocketInstance.class.getClassLoader().getResourceAsStream(filename));
            return config;
        } catch (IOException e) {
            throw new ConfigException("Need A Config File: client.properties At ClassPath", e);
        }
    }
}
